<!doctype html>
<html lang="en">
  <head>
    <link rel="stylesheet" href="public/styles/main.css">
  </head>
  <body>

    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Add your site or application content here -->
    <div class="container">
      <?php include(__DIR__ .'/private/header.php'); ?>
      <div class="row col-md-12" style="padding-right: 0px">
        <?php include(__DIR__ .'/private/catalogs-index.php'); ?>
      </div>
    </div>

    <?php include(__DIR__ .'/private/footer.php'); ?>
</body>
</html>
