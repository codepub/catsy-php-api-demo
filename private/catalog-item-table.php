<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    $familyTables = \webCatalogs\getFamilyTables($familyId, $languageId);
//    Showing the first table as an example
    $table = $familyTables[0];
    if (isset($table)) {
        $rowCount = $table->headerRowCount + $table->bodyRowCount;
        $columnCount = $table->columnCount;
    }
    ?>
</head>

<body>
    <div class="container">
        <div class="row col-md-12" style="padding: 0px; font-size: 10px">
            <?php if ($rowCount > 0 && $columnCount): ?>
                <table class="table table-bordered table-striped table-sm">
                    <thead>
                        <?php
                        for ($i=1; $i<=$table->headerRowCount; $i++) {
                            echo "<td style='background-color: #FFE793'></td>";
                            foreach($table->cells as $cell) {
                                if ($cell->rowId == $i) {
                                    $headerRow = "<td colspan='".$cell->colLength."' rowspan='".$cell->rowLength."' style='background-color: #FFE793'>$cell->value</td>";
                                    echo $headerRow;
                                }
                            }
                            echo "</tr>";
                        }
                        ?>
                    </thead>
                    <tbody>
                        <?php
                            for ($j=$table->headerRowCount+1; $j<=$table->bodyRowCount; $j++) {
                                $row = $j-$table->headerRowCount;
                                echo "<tr> \n <td style='background-color: #FFE793'>$row</td>";
                                foreach($table->cells as $cell) {
                                    if ($cell->rowId == $j) {
                                        $href = "catalog-item-detail.php?itemId=$cell->itemId&languageId=$languageId&multiLanguage=$multiLanguage";
                                        $value = "<a href=$href target='_blank' style='color:black'>$cell->value</a>";
                                        $bodyRow = "<td colspan='".$cell->colLength."' rowspan='".$cell->rowLength."'>$value</td>";
                                        echo $bodyRow;
                                    }
                                }
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            <?php else: ?>
                <div><span>No item results!</span></div>
            <?php endif; ?>
        </div>
    </div>

</body>