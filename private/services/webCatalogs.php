<?php

namespace webCatalogs;

require_once (__DIR__."/../services/apsivaApi.php");

define("MISSING_CATALOG", NULL);
define("MISSING_CATEGORY", NULL);
define("MISSING_RESULT", NULL);
define("NO_LIST", NULL);

function allCatalogs() {
	static $webCatalogs;
	global $apsivaApi;

	if (!isset($webCatalogs)) {
		$webCatalogs = array();

		$response = $apsivaApi->sendRequest("/catalogs", "get");
		$webCatalogs = $response->catalogs;
	}

	return $webCatalogs;
}

function getCatalog($catalogId) {
	global $apsivaApi;
	$params = array("includeSubCategories" => "true");
	$catalog = $apsivaApi->sendRequest("/catalogs/{$catalogId}", "get", $params)->catalog;
	return $catalog;
}

function getCategoryName($category, $languageId, $languages) {
	global $apsivaApi;
//	$languages = languages();
	$defaultLanguage = getDefaultLanguage($languages);
	$languageId = isset($languageId) ? $languageId : $defaultLanguage->id;

//	$category = $apsivaApi->sendRequest("/categories/{$categoryId}", "get")->category;
	if (isset($category->values)) {
		foreach ($category->values as $value) {
			if ($value->languageId == $languageId && $value->fieldName == "name") {
				$categoryName = $value->value;
				break;
			}
		}
	}
	if (!isset($categoryName)) {
		$categoryName = $category->name;
	}
	return $categoryName;
}

//function catalogCategories($catalogId) {
//	global $apsivaApi;
//	static $cache = array();
//
//	if (isset($cache[$catalogId])) {
//		return $cache[$catalogId];
//	}
//
//	$allCatalogs = allCatalogs();
//	$params = array("includeDetails" => "true", "includeSubCategories" => "true");
//	$categories = $apsivaApi->sendRequest("/catalogs/{$catalogId}/categories", "get", $params)->categories;
//
//	$catalog = NULL;
//	$cwd = $_GET["cwd"];
//
//	foreach ($allCatalogs as $meta) {
//		if ($meta->id == $catalogId) {
//			$catalog = $meta;
//			break;
//		}
//	}
//
//	if (!$catalog) {
//		throw Exception('Catalog is missing!');
//	}
//
//	return $categories;
//}

function languages() {
	global $apsivaApi;
	$languages = $apsivaApi->sendRequest("/master/languages", "get")->languages;
	return $languages;
}

function getDefaultLanguage($languages) {
	if (isset($languages)) {
		foreach ($languages as $language) {
			if ($language->defaultLanguage == 1) {
				return $language;
			}
		}
	}
}

function categoryResultsById($categoryId, $languageId, $languages) {
	global $apsivaApi;

//	$languages = languages();
	$defaultLanguage = getDefaultLanguage($languages);
	$languageId = isset($languageId) ? $languageId : $defaultLanguage->id;

	$params = array("languageId" => $languageId);
	$rawResults = $apsivaApi->sendRequest("/categories/{$categoryId}/results", "get", $params)->results;
	$missingImage = "cf-cm2/images/image-missing.png";

	foreach ($rawResults as $result) {
		$result->thumbnailUrl = APSIVA_ASSETS_HOST . '/'.$missingImage;
		$values = isset($result->values) ? $result->values : [];
		foreach ($values as $value) {
			if (isset($languageId)) {
				$result->name = $value->value;
				break;
			}
		}

		if ($result->assets) {
			foreach ($result->assets as $asset) {
				if ($asset->url) {
					$result->thumbnailUrl = $apsivaApi->assetUrl($asset->url);
					break;
				}
			}
		}
	}

	return $rawResults;
}

function getFamilyDetail($groupId) {
	global $apsivaApi;
	$familyDetail = $apsivaApi->sendRequest("/itemGroups/{$groupId}", "get")->itemGroup;
	return $familyDetail;
}

function getFamilyFieldName($fieldId) {
	global $apsivaApi;
	$itemGroupField = $apsivaApi->sendRequest("/master/itemGroupFields/{$fieldId}", "get")->itemGroupField;
	if (isset($itemGroupField)) {
		foreach ($itemGroupField->details as $detail) {
			if ($detail->value) {
				$familyFieldName = $detail->value;
				break;
			}
		}
	}
	return $familyFieldName;
}

function getFamilyTables($groupId, $languageId) {
	global $apsivaApi;
	$languages = languages();
	$defaultLanguage = getDefaultLanguage($languages);
	$languageId = isset($languageId) ? $languageId : $defaultLanguage->id;

	$params = array("languageId" => $languageId);
	$familyTables = $apsivaApi->sendRequest("/itemGroups/{$groupId}/fullDetail", "get", $params)->itemGroup->tables;
	return $familyTables;
}

function getItemDetail($itemId, $languageId) {
	global $apsivaApi;
	$languages = languages();
	$defaultLanguage = getDefaultLanguage($languages);
	$languageId = isset($languageId) ? $languageId : $defaultLanguage->id;

	$params = array("languageId" => $languageId);
	$item = $apsivaApi->sendRequest("/items/{$itemId}", "get", $params)->item;

	$missingImage = "cf-cm2/images/image-missing.png";
	$item->thumbnailUrl = APSIVA_ASSETS_HOST . '/'.$missingImage;
	return $item;
}

function getImage() {
	$hoistingImage = array(
		"https://prd02.apsiva.net/344/Hoisting/LHH_TN.jpg",
		"https://prd02.apsiva.net/344/Hoisting/hoist_TN.jpg"
	);
	$pullingImage = array(
		"https://prd02.apsiva.net/344/Pulling/Griphoist_TU_TN.jpg",
		"https://prd02.apsiva.net/344/Pulling/wireRopeWinchDtl_TN.jpg"
	);
	$images = array(
		$hoistingImage,
		$pullingImage
	);
	return $images;
}