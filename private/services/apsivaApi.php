<?php
include(__DIR__ .'/../../config.php');

define("API_TOKEN", Conf::API_TOKEN);
define("READONLY_USERNAME", Conf::READONLY_USERNAME);
define("READONLY_PASSWORD", Conf::READONLY_PASSWORD);
define("CLIENT_ID", Conf::CLIENT_ID);

define("APSIVA_HOST", Conf::APSIVA_HOST);
define("APSIVA_ASSETS_HOST", Conf::APSIVA_ASSETS_HOST);

date_default_timezone_set('UTC');
ini_set('memory_limit', '-1');

function cfcm_url($url) {
	return APSIVA_HOST . "/cf-cm2/" . ltrim($url, "/");
}

function get_ms_time() {
	return round(microtime(true) * 1000);
}

function apsiva_canonical($str) {
	$str = strtolower($str);
	$str = preg_replace('/[^a-z0-9<>]+/' , '-', $str);
	$str = preg_replace('/-?<-?/' , '-lt-', $str);
	$str = preg_replace('/-?>-?/' , '-gt-', $str);
	$str = rtrim($str, '-');
	$str = ltrim($str, '-');

	return $str;
}

class ApsivaAPI {
	public $apiToken = API_TOKEN;
	public $clientId = CLIENT_ID;

	public function __construct() {
		$apiToken = API_TOKEN;
	}

	protected function refreshApiToken() {
		$username = READONLY_USERNAME;
		$password = READONLY_PASSWORD;
		$url = cfcm_url("/json/{$this->clientId}/login.json?username=$username&pass=$password");

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$response = curl_exec($ch);
		$loginJson = json_decode($response);
		curl_close($ch);

		if (isset($loginJson->error) && !is_null($loginJson->error)) {
			throw new Exception("Catsy Login Error: $loginJson->error");
		}

		if (isset($loginJson->api_token) && !is_null($loginJson->api_token)) {
			$this->apiToken = $loginJson->api_token;
			$_SESSION['api_token'] = $loginJson->api_token;
		} else {
			$this->apiToken = null;
		}
	}

	/**
	 * @param {string} $params
	 * @return string
	 */
	private function formEncode($params) {
		$formParams = "";
		foreach($params as $key => $value) {
			$formParams .= $key . '=' . rawurlencode($value) . '&';
		}

		$formParams = rtrim($formParams, '&');
		return $formParams;
	}

	public function assetUrl($relativeUrl) {
		return APSIVA_ASSETS_HOST . '/' . CLIENT_ID . '/' . $relativeUrl;
	}

	/**
	 * Measures how much time it took to complete a request in ms
	 * @param string $apiMethod
	 * @param string [$method]
	 * @param array [$params]
	 * @param bool [$retryOnFail]
	 * @return int Time in milliseconds it too kto compelte the request
	 * @throws Exception
	 */
	public function measureRequest($apiMethod, $method = "get", $params = array(), $retryOnFail = true) {
		$startTime = get_ms_time();
		$this->sendRequest($apiMethod, $method, $params, $retryOnFail);
		$endTime = get_ms_time();

		return $endTime - $startTime;
	}

	/**
	 * @param string $apiMethod
	 * @param string [$method]
	 * @param array [$params]
	 * @param bool [$retryOnFail]
	 * @return mixed
	 * @throws Exception
	 */
	public function sendRequest($apiMethod, $method = "get", $params = array(), $retryOnFail = true) {
		$absUrl = cfcm_url('/api' . $apiMethod);
		$isPost = strcasecmp($method, 'post') == 0;

		if (is_null($this->apiToken)) {
			$this->refreshApiToken();
		}

		if (!$retryOnFail && is_null($this->apiToken)) {
			throw new Exception("Missing API Token");
		}

		$ch = curl_init();
		$formParams = $this->formEncode($params);

		if ($isPost) {
			curl_setopt($ch,CURLOPT_POST, count($formParams));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $formParams);
		} else {
			$absUrl .= '?' . $formParams;
		}

		curl_setopt($ch, CURLOPT_URL, $absUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$this->apiToken}"));

		$startTime = get_ms_time();
		$response = curl_exec($ch);
		$endTime = get_ms_time();

		//var_dump($count);
//		if ($count == 2) {
//			var_dump($apiMethod);
//			var_dump($method);
//			var_dump($params);
//			var_dump($endTime - $startTime);
//			die;
//		}
		$jsonData = null;
		$errorInfo = null;

		if ($response == false) {
			//curl_error($ch) . '<br/>';
			// No need to retry from this one
			$errorInfo = new Exception("CURL error: " . curl_getinfo($ch) . "(" . curl_errno($ch) . ")");
			//curl_close($ch);
		} else {
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			if ($httpCode == 200) {
				$jsonData = json_decode($response);
			} else {
				$errorInfo = "HTTP error: $httpCode";
			}

			curl_close($ch);
		}

		if (is_null($errorInfo)) {
			if (is_null($jsonData)) {
				// No need to retry from this one. JSON was invalid.
				throw new Exception("Invalid JSON");
			}

			// Response was valid but something went wrong.
			if (isset($jsonData->error) && !is_null($jsonData->error)) {
				if ($retryOnFail) {
					// Try to get the apiToken again and retry the request.
					$this->refreshApiToken();
					$this->sendRequest($apiMethod, $method, $params, false);
				} else {
					throw new Exception("Server error: $jsonData->error");
				}
			}
		} else if ($retryOnFail) {
			// Try to get the apiToken again and retry the request.
			$this->refreshApiToken();
			$this->sendRequest($apiMethod, $method, $params, false);
		} else {
			throw new Exception($errorInfo);
		}

		return $jsonData;
	}
}

session_start();
$apsivaApi = new ApsivaAPI();