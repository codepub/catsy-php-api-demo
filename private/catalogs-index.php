<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../public/styles/main.css">
    <?php
    require_once( "private/services/webCatalogs.php" );
    $webCatalogs = \webCatalogs\allCatalogs();
    ?>
</head>

<body>
<div><h3 class="text-center">Catalogs List</h3></div>
<?php foreach($webCatalogs as $result): ?>
    <div class="col-md-12">
<!--        Temporary Hard code of default language selection-->
        <?php
            if ($result->id == 4004624740) {
                $languageId = 1;
            } else if ($result->id == 4004624721) {
                $languageId = 7;
            } else if ($result->id == 4004621464) {
                $languageId = 1029;
            }
        ?>
        <form class="form-inline catalogs-index" action="private/catalog-detail.php" method="get">
            <div class="form-group">
                <h4><?= $result->name ?><span style="font-size: 14px; color: grey">&nbsp;(ID: <?= $result->id ?>)</span></h4>
                <input type="text" name="catalogId" value="<?= $result->id ?>" style="display: none">
                <input type="text" name="languageId" value="<?= $languageId ?>" style="display: none">
            </div>
            <div class="form-group pull-right">
                <div class="checkbox" style="margin-right: 10px">
                    <label>
                        <input type="checkbox" name="multiLanguage"> Multi-language
                    </label>
                </div>
                <button type="submit" class="btn btn-default">Enter into Website</button>
            </div>
        </form>
    </div>
<?php endforeach; ?>
</body>