<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    require_once( "services/webCatalogs.php" );
    $categoryId = $_GET["categoryId"];
    $familyId = $_GET["familyId"];
    $languageId = $_GET["languageId"];
    $multiLanguage = $_GET["multiLanguage"];
    $path = $_GET["path"];
    $href = "catalog-family-detail.php?categoryId=$categoryId&familyId=$familyId&multiLanguage=$multiLanguage&path=$path";
    ?>
</head>

<body>
<div class="se-pre-con"><span class="pre-word">Loading page...</span></div>
<div class="container">
    <?php include(__DIR__ .'/header.php'); ?>
    <?php
    $catalogResults = \webCatalogs\categoryResultsById($categoryId, $languageId, $languages);
    $familyDetail = \webCatalogs\getFamilyDetail($familyId);
    foreach ($catalogResults as $item) {
        if ($item->id == $familyId) {
            $family = $item;
            break;
        }
    }
    if (isset($familyDetail) && isset($familyDetail->values)) {
        $familyValues = $familyDetail->values;
    }
    $catalogFamilyDetailPath = $path.' / '.$family->name;
    ?>
    <div class="row col-md-12" style="padding-right: 0px">
        <div class="col-lg-3 col-md-3">
            <a class="wraptocenter" target="_blank">
                <img src="<?= $family->thumbnailUrl ?>" alt="">
            </a>
        </div>
        <div class="col-lg-9 col-md-9" style="padding-right: 0px">
            <div class="family-title-lg">
                <span><?= $family->name ?></span>
            </div>
            <div id="catalog-family-detail-path" style="font-size: 12px; color: grey; margin-bottom: 10px;"><?= $path ?></div>
            <?php foreach($familyValues as $value): ?>
                <div>
                    <?php if ($value->languageId == $languageId): ?>
                        <?php if (isset($value->fieldName)): ?>
                            <span><?= $value->fieldName ?>:</span>
                        <?php else: ?>
                            <span><?= \webCatalogs\getFamilyFieldName($value->fieldId) ?>:</span>
                        <?php endif; ?>
                        <span><?= $value->value ?></span>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="row col-md-12">
        <?php include(__DIR__ .'/catalog-item-table.php'); ?>
    </div>
</div>
<?php include(__DIR__ .'/footer.php'); ?>

</body>