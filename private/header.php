<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../public/styles/main.css">
    <?php
    require_once( "services/webCatalogs.php" );
    $languages = \webCatalogs\languages();
    foreach ($languages as $lang) {
        if ($lang->id == $languageId) {
            $languageName = $lang->name;
            break;
        }
    }
    $languageName = $languageName ? $languageName : 'Language';
    ?>
</head>
<body>
    <div class="header">
        <div class="btn-group pull-right">
            <?php if ($multiLanguage == 'on') : ?>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= $languageName ?>
                </button>
                <div class="dropdown-menu pull-right">
                    <?php foreach($languages as $lang): ?>
                        <li>
                            <a class="dropdown-item" href="<?= $href.'&languageId='.$lang->id ?>"><?= $lang->name ?></a>
                        </li>
                    <?php endforeach; ?>
                </div>
            <?php endif?>
        </div>
        <ul class="nav nav-pills pull-right">
            <?php if ($multiLanguage == 'on') : ?>
                <li><a href="#">Home</a></li>
            <?php else:?>
                <li class="active"><a href="#">Home</a></li>
            <?php endif?>
            <li><a href="#/about">About</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
        <h3 class="text-muted">Header</h3>
    </div>

</body>
</html>
