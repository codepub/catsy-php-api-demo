<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    require_once( "services/webCatalogs.php" );
    $itemId = $_GET["itemId"];
    $languageId = $_GET["languageId"];
    $multiLanguage = $_GET["multiLanguage"];
    $item = \webCatalogs\getItemDetail($itemId, $languageId);
    $href = "catalog-item-detail.php?itemId=$itemId&languageId=$languageId&multiLanguage=$multiLanguage";
    ?>
</head>

<body>
    <div class="container">
        <?php include(__DIR__ .'/header.php'); ?>
        <div class="row col-md-12" style="padding-right: 0px">
            <div class="col-lg-3 col-md-3">
                <a class="wraptocenter" target="_blank">
                    <img src="<?= $item->thumbnailUrl ?>" alt="">
                </a>
            </div>
            <div class="col-lg-9 col-md-9" style="padding-right: 0px">
                <div class="family-title-lg">
                    <span><?= $item->number ?></span>
                </div>
            </div>
        </div>

        <div class="row col-md-12 col-sm-12" style="padding-right: 0px">
            <h4>Item Detail</h4>
            <?php if (isset($item->values)): ?>
                <?php foreach($item->values as $value): ?>
                    <div>
                        <span><?= $value->fieldName ?>:</span>
                        <span><?= $value->value ?></span>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>

    <?php include(__DIR__ .'/footer.php'); ?>

</body>