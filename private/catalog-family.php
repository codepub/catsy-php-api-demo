<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    require_once( "services/webCatalogs.php" );
    $categoryId = $_GET["categoryId"];
    $languageId = $_GET["languageId"];
    $multiLanguage = $_GET["multiLanguage"];
    $catalogFamilyPath = urldecode($_GET["catalogFamilyPath"]);
    $encodePath = urlencode($catalogFamilyPath);
    $href = "catalog-family.php?categoryId=$categoryId&catalogFamilyPath=$encodePath&multiLanguage=$multiLanguage";
    ?>
</head>

<body>
    <div class="se-pre-con"><span class="pre-word">Loading page...</span></div>
    <div class="container">
        <?php include(__DIR__ .'/header.php'); ?>
        <?php
        $defaultLanguage = \webCatalogs\getDefaultLanguage($languages);
        $languageId = isset($languageId) ? $languageId : $defaultLanguage->id;
        $catalogResults = \webCatalogs\categoryResultsById($categoryId, $languageId, $languages);
        ?>
        <div class="row col-md-12" style="font-size: 12px; padding-left: 15px">
            <div id="catalog-family-path" style="color: grey"><?= $catalogFamilyPath ?></div>
            <?php if ($catalogResults): ?>
                <div>Showing <?= count($catalogResults) ?> results. </div>
                <?php foreach($catalogResults as $result): ?>
                    <div class="col-md-4 family-box" style="padding: 0px">
                        <div class="thumbnail-box">
                            <a href="catalog-family-detail.php?categoryId=<?php echo $categoryId; ?>&familyId=<?php echo $result->id; ?>&languageId=<?php echo $languageId; ?>&multiLanguage=<?php echo $multiLanguage; ?>&path=<?php echo $catalogFamilyPath; ?>" class="wraptocenter">
                                <img src="<?= $result->thumbnailUrl ?>" class="category-family-detail" alt="">
                            </a>
                            <div class="family-title"><?= $result->name ?></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div>Showing 0 result. </div>
            <?php endif; ?>
        </div>
    </div>

    <?php include(__DIR__ .'/footer.php'); ?>
</body>