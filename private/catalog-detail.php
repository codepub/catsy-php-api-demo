<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../public/styles/main.css">
    <?php
    require_once( "services/webCatalogs.php" );
    $catalogId = $_GET["catalogId"];
    $multiLanguage = $_GET["multiLanguage"];
    $languageId = $_GET["languageId"];
    $catalog = \webCatalogs\getCatalog($catalogId);
    $categories = $catalog->children[0]->children;
//    $categories = \webCatalogs\catalogCategories($catalogId);
    $href = "catalog-detail.php?catalogId=$catalogId&multiLanguage=$multiLanguage";
    ?>
</head>

<body>
    <div class="se-pre-con"><span class="pre-word">Loading page...</span></div>
    <div class="container">
        <?php include(__DIR__ .'/header.php'); ?>

        <div class="">
            <?php
            echo "<h4>$catalog->name</h4>";
            $images = \webCatalogs\getImage();
            $imageArrayIndex = 0;
            foreach ($categories as $category) {
                $image = $images[$imageArrayIndex];
                $imageArrayIndex = $imageArrayIndex + 1;

                $tmpHref = "#$category->id";
                $catalogName = \webCatalogs\getCategoryName($category, $languageId, $languages);
                echo "<div class='categories row' style='margin: 0px;'><div><a data-toggle='collapse' href=$tmpHref>$catalogName";
                if ($category->childrenCount) {
                    echo "<span> ($category->childrenCount)</span>";
                } else {
                    echo "<span> (0)</span>";
                }
                echo "</a></div><div id=$category->id class='collapse in'>";
                if ($category->childrenCount) {
                    $num = 0;
                    foreach ($category->children as $child) {
                        $childName = \webCatalogs\getCategoryName($child, $languageId, $languages);
                        $encodePath = urlencode("$catalog->name / $catalogName / $childName");
                        $familyHref = "catalog-family.php?categoryId=$child->id&languageId=$languageId&catalogFamilyPath=$encodePath&multiLanguage=$multiLanguage";
                        $url = $image[$num];
                        if (!isset($url)) {
                            $url = "https://prd02.apsiva.net/cf-cm2/images/image-missing.png";
                        }
                        echo "<div class='col-md-4 thumbnail-box'><a href=$familyHref class='wraptocenter'>
                              <img src=$url class='category-family-detail' alt=''><br></a>
                              <div class='family-title' id=$child->id>$childName";
                        if ($child->resultsCount) {
                            echo "<span> ($child->resultsCount)</span>";
                        } else {
                            echo "<span> (0)</span>";
                        }
                        echo "</div></div>";
                        $num = $num + 1;
                    }
                }
                echo "</div></div><br>";
            }
            ?>
        </div>
    </div>
    <?php include(__DIR__ .'/footer.php'); ?>

</body>